variable "prefix" {
  default = "raad"
}

# Variables for tagging our instance.
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "seraphinclayton@gmail.com"
}